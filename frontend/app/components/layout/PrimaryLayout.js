import styled from 'styled-components';

import media from '../../utils/media';

export default styled.main`
  display: flex;
  font-size: 1.5rem;
  margin: 0 auto;
  max-width: 90rem;
  padding: 2rem 0;
  ${media.m`
    flex-direction: column;
  `}
`