import styled from 'styled-components';

const Upvote = styled.button`
  background: none;
  border: solid 1px #e2e2e2;
  border-radius: 50%;
  height: 4.5rem;
  margin: 0 1rem;
  position: relative;
  width: 4.5rem;
  &:before {
    content: "👍";
    font-size: 2.5rem;
    left: 50%;
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
  }
`

const Downvote = styled(Upvote)`
  &:before {
    content: "👎";
  }
`

export { Downvote, Upvote };
