import React, { Fragment } from 'react';
import styled, { keyframes } from 'styled-components';

import media from '../../utils/media'

import CompanyProfile from './CompanyProfile';
import { Downvote, Upvote } from '../buttons/VoteButtons';

const CompanyRater = styled.section`
  flex-grow: 1;
  padding: 1rem 8rem 1rem 1rem;
  text-align: center;
  ${media.m`
    padding: 1rem;
  `}
`

const Finished = styled.p`
  font-size: 1.6rem;
  margin: 5rem 0 0 0;
`

const wiggle = keyframes`
  from {
    transform: rotate(-25deg);
  } to {
    transform: rotate(-10deg);
  }
`

const FinishedIcon = styled.span`
  animation: ${wiggle} .2s infinite alternate ease-in-out;
  display: inline-block;
  margin-left: .6rem;
`

const Title = styled.h1`
  margin: 0 0 1.5rem 0;
`

const Rater = styled.div``

export default ({ company, rateCompany }) => (
  <CompanyRater>
    <Title>
      Tinder for Companies
    </Title>
    {company ? (
      <Fragment>
        <CompanyProfile company={company} />
        <Rater>
          <Upvote aria-label="upvote" onClick={() => rateCompany(1)} />
          <Downvote aria-label="downvote" onClick={() => rateCompany(0)} />
        </Rater>
      </Fragment>
    ) : (
      <Finished>
        All done! <FinishedIcon>🎉</FinishedIcon>
      </Finished>
    )}
  </CompanyRater>
)
