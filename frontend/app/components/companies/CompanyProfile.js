import React from 'react';
import styled from 'styled-components';

const CompanyProfile = styled.section`
  border: solid 1px #e2e2e2;
  margin: 8rem 0 3rem 0;
  padding: 7rem 2rem 3.5rem 2rem;
  position: relative;
  text-align: center;
`

const LogoWrapper = styled.div`
  background: #fff;
  border: solid 1px #e2e2e2;
  height: 10rem;
  left: 50%;
  position: absolute;
  top: -5rem;
  transform: translateX(-50%);
  width: 10rem;
`

const Logo = styled.img`
  height: auto;
  left: 50%;
  position: absolute;
  max-height: 90%;
  max-width: 90%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: auto;
`

const Name = styled.h2`
  font-size: 2.4rem;
  font-weight: 400;
  margin: 0 0 1.5rem 0;
`

const Location = styled.p`
  color: #555;
  margin: 0;
  font-size: 1.3rem;
  font-style: italic;
  font-weight: 600;
`

const Description = styled.p`
  margin-bottom: 0;
`

export default ({ company: { description, location, logo, name } }) => (
  <CompanyProfile>
    <LogoWrapper>
      <Logo alt={name} src={logo} />
    </LogoWrapper>
    <Name>
      {name}
    </Name>
    <Location>
      Located in {location}
    </Location>
    <Description>
      {description}
    </Description>
  </CompanyProfile>
)
