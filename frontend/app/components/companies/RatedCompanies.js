import React from 'react';
import styled, { keyframes } from 'styled-components';

import media from '../../utils/media';

const RatedCompanies = styled.section`
  flex-basis: 28rem;
  flex-grow: 0;
  flex-shrink: 0;
  padding: 2rem 1rem 1rem 1rem;
  ${media.m`
    flex-basis: auto;
    text-align: center;
  `}
`

const Title = styled.h2`
  font-size: 1.5rem;
  margin: 0 0 1.5rem 0;
`

const Empty = styled.p`
  font-size: 1.4rem;
  margin: 1.5rem 0;
`

const Companies = styled.ul`
  font-size: 1.4rem;
  list-style-type: none;
  margin: 0;
  padding: 0;
`

const fadeDown = keyframes`
  from {
    opacity: 0;
    transform: translateY(-2rem);
  } to {
    opacity: 1;
    transform: translateY(0);
  }
`

const Company = styled.li`
  animation: ${fadeDown} .3s;
  margin: .8rem 0;
  &:first-child {
    margin-top: 0;
  }
  &:last-child {
    margin-bottom: 0;
  }
`

export default ({ companies }) => (
  <RatedCompanies>
    <Title>
      Rated Companies
    </Title>
    {companies.length > 0 ? (
      <Companies>
        {companies.map(({ id, name, vote }) => (
          <Company key={id}>
            {name} {vote === 1 ? '👍' : '👎'}
          </Company>
        ))}
      </Companies>
    ) : (
      <Empty>
        You haven't rated any companies yet.
      </Empty>
    )}
  </RatedCompanies>
)
