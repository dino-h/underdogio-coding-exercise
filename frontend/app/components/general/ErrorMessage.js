import styled from 'styled-components';

export default styled.p`
  color: red;
  font-size: 2rem;
  padding: 5rem 2rem;
  text-align: center;
`
