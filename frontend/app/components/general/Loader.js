import React from 'react';
import styled, { keyframes } from 'styled-components';

import media from '../../utils/media';

const Loader = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: center;
  padding: 4.5rem 8rem 1rem 1rem;
  ${media.m`
    padding: 4.5rem 0;
  `}
`

const bounce = keyframes`
  to {
    opacity: 0.1;
    transform: translate3d(0, -.5rem, 0);
  }
`

const Circle = styled.div`
  animation: ${bounce} .6s infinite alternate;
  background: #222;
  border-radius: 50%;
  height: .5rem;
  margin: 0 0.5rem;
  width: .5rem;
  &:nth-child(2) {
    animation-delay: .2s
  }
  &:nth-child(3) {
    animation-delay: .4s
  }
`

export default () => (
  <Loader>
    <Circle />
    <Circle />
    <Circle />
  </Loader>
)
