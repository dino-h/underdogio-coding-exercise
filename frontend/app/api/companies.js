import Api from './api';

class CompaniesApi extends Api {

  /**
   * Get a list of companies.
   * @returns {Promise}
   */
  getCompaniesRequest() {
    return fetch(`${this.apiUrl}/companies`, this.defaultConfig)
      .then(response => {
        if (!response.ok) {
          return response.text()
            .then(error => {
              throw JSON.parse(error);
            });
        }
        return response.json();
      });
  }

}

export default CompaniesApi;
