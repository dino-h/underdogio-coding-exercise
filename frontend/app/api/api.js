class Api {

  constructor() {
    this.apiUrl = 'http://localhost:3000';

    this.defaultHeaders = {
      'Content-Type': 'application/json; charset=utf-8'
    };

    this.defaultConfig = {
      headers: this.defaultHeaders,
      mode: 'cors'
    };
  }

}

export default Api;
