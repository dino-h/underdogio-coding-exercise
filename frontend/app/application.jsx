import React, { Component } from 'react';

import CompaniesApi from './api/companies';

import CompanyRater from './components/companies/CompanyRater';
import RatedCompanies from './components/companies/RatedCompanies';
import Loader from './components/general/Loader';
import ErrorMessage from './components/general/ErrorMessage';
import PrimaryLayout from './components/layout/PrimaryLayout';

class Application extends Component {

  constructor() {
    super();

    this.state = {
      companies: [],
      currentCompany: null,
      error: null,
      loading: true,
      ratedCompanies: []
    };

    this.companiesApi = new CompaniesApi;

    this.rateCompany = this.rateCompany.bind(this);
  }

  /**
   * Set the "vote" property of the current company, and 
   * add the company to the "ratedCompanies" state array.
   * @param {number} vote - 0 for rejecting, 1 for accepting
   */
  rateCompany(vote) {
    this.setState(({ companies, currentCompany, ratedCompanies }) => {
      currentCompany = {...currentCompany, vote};
      ratedCompanies = [...ratedCompanies, currentCompany];

      return {
        currentCompany: companies[ratedCompanies.length]
          ? companies[ratedCompanies.length]
          : null,
        ratedCompanies
      };
    });
  }

  componentDidMount() {
    this.companiesApi
      .getCompaniesRequest()
      .then(companies => this.setState({
        companies: companies,
        currentCompany: companies[0],
        loading: false
      }))
      .catch(({ message }) => this.setState({
        error: message,
        loading: false
      }));
  }

  render() {
    const { currentCompany, error, loading, ratedCompanies } = this.state;

    if (error) {
      return (
        <ErrorMessage>
          {error} Please try again later...
        </ErrorMessage>
      );
    }

    return (
      <PrimaryLayout>
        {!loading 
          ? <CompanyRater company={currentCompany} rateCompany={this.rateCompany} />
          : <Loader />
        }
        <RatedCompanies companies={ratedCompanies} />
      </PrimaryLayout>
    );
  }

}

export default Application;
