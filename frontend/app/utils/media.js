import { css } from 'styled-components';

const sizes = {
  l: 992,
  m: 768,
  s: 576
};

const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (max-width: ${sizes[label] / 16}em) {
      ${css(...args)}
    }
  `
  return acc;
}, {});

export default media;
