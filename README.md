# Underdog.io Coding Exercise

## Frontend

### Setup

#### Install:

```bash
npm install
```

#### Run:

```bash
npm run dev
```

### Description

A very simple React application, no store management or http dependencies. Styling is handled with a couple of global styles that aid as CSS reset (normalize.css) and simple global CSS rules (global.css). Everything else is styled using styled-components, a CSS-in-JS library. The app is responsive.

When loading the app, a loader is shown. Then, the application makes an http request to fetch the list of companies. If the request does not succeed, an error message coming from the server is shown. If the request succeeds, company voting can start.

## Backend

### Setup

#### Install:

```bash
npm install
```

#### Run:

```bash
node index.js
```

### Description

A super simple server listening on `localhost:3000`. Maybe too simple. It uses Express. The only route available is `/companies`, which returns a list of all the companies available in the `companies.json` file. City codes are also transformed to full city names. Only New York and San Francisco are supported.

This solution DOES NOT scale. Companies should be returned by the API one by one, or paginated with a maximum number of entries per page. 
