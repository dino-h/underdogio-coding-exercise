const usCityCodesDictionary = {
  nyc: 'New York City',
  sf: 'San Francisco'
};

function translateCompaniesCityCodes(companies, dictionary) {
  return companies.map(company => {
    company.location = dictionary[company.location];
    return company;
  });
}

module.exports = {
  translateCompaniesCityCodes,
  usCityCodesDictionary
};
