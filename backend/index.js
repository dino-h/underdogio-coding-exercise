const compression = require('compression');
const cors = require('cors');
const express = require('express');

const { translateCompaniesCityCodes, usCityCodesDictionary } = require('./cities');

const companies = require('./companies.json');
const port = 3000;

const app = express();

app.use(compression());
app.use(cors());

const transformedCompanies = translateCompaniesCityCodes(companies, usCityCodesDictionary);

app.get('/companies', (req, res) => {
  if (!transformedCompanies) {
    return res.status(404).send({
      message: 'Data not found.'
    });
  }

  return res.send(transformedCompanies);
});

app.listen(port, () => console.log(`App listening on port ${port}...`));
